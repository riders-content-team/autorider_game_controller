import math
from geometry_msgs.msg import Point

class LineAnalyzer:

    def __init__(self):
        # parameters
        self.maximum_point_skip_allowed = 25 # if we skip more than this we are out of sequence
        self.out_of_track_radius = 1.0 # if we depart more than this from course we are out of bounds

        # Line Points are x0, y0, x1, y1, x2, y2, etc ...
        self.linePoints = [
            3.987762, 2.356250,
            3.982225, 2.231250,
            3.967863, 2.106250,
            3.943029, 1.979670,
            3.900426, 1.837317,
            3.848255, 1.713248,
            3.778186, 1.592840,
            3.690974, 1.477314,
            3.587757, 1.368441,
            3.476889, 1.282631,
            3.368458, 1.213522,
            3.242428, 1.154628,
            3.097008, 1.109840,
            2.966930, 1.081371,
            2.829086, 1.057573,
            2.693750, 1.044500,
            2.568750, 1.040712,
            2.443750, 1.042863,
            2.318750, 1.051837,
            2.195027, 1.066970,
            2.068750, 1.080050,
            1.944790, 1.091141,
            1.818750, 1.102600,
            1.693750, 1.106838,
            1.568750, 1.116788,
            1.439054, 1.108217,
            1.299832, 1.087892,
            1.164272, 1.045337,
            1.042352, 0.992392,
            0.913658, 0.921412,
            0.798593, 0.832939,
            0.705327, 0.738269,
            0.625806, 0.631020,
            0.556825, 0.499521,
            0.517657, 0.379777,
            0.495587, 0.239129,
            0.493362, 0.106250,
            0.525567, -0.001705,
            0.586642, -0.115155,
            0.658073, -0.213698,
            0.760018, -0.300598,
            0.872775, -0.362713,
            0.993750, -0.407875,
            1.120628, -0.434213,
            1.243750, -0.453038,
            1.368750, -0.462862,
            1.493750, -0.465438,
            1.618750, -0.462938,
            1.743750, -0.455813,
            1.866758, -0.443028,
            1.981250, -0.425075,
            2.099682, -0.412054,
            2.218750, -0.399975,
            2.343750, -0.387275,
            2.468750, -0.373488,
            2.593750, -0.367687,
            2.718750, -0.376750,
            2.841384, -0.403436,
            2.941916, -0.439695,
            3.047650, -0.500400,
            3.130007, -0.594993,
            3.173820, -0.715370,
            3.182452, -0.845879,
            3.158649, -0.989500,
            3.117873, -1.107785,
            3.054079, -1.235690,
            2.977965, -1.359762,
            2.901106, -1.469066,
            2.800449, -1.582666,
            2.688509, -1.678328,
            2.575953, -1.765730,
            2.462507, -1.857298,
            2.349028, -1.956481,
            2.254616, -2.061139,
            2.167318, -2.173807,
            2.097052, -2.302752,
            2.066663, -2.431250,
            2.076789, -2.555228,
            2.117620, -2.679787,
            2.175609, -2.776678,
            2.260268, -2.862723,
            2.357905, -2.931707,
            2.470120, -2.982705,
            2.585191, -3.025338,
            2.706250, -3.056912,
            2.831250, -3.082813,
            2.956250, -3.106188,
            3.081250, -3.125025,
            3.206250, -3.138488,
            3.331250, -3.152175,
            3.458246, -3.171507,
            3.585455, -3.212443,
            3.696235, -3.276258,
            3.773965, -3.377773,
            3.821116, -3.496308,
            3.818017, -3.632826,
            3.751179, -3.758274,
            3.650026, -3.857313,
            3.538486, -3.933114,
            3.417548, -3.995793,
            3.292256, -4.044771,
            3.154608, -4.081754,
            3.018750, -4.108200,
            2.889079, -4.129326,
            2.749667, -4.149534,
            2.618750, -4.170588,
            2.493750, -4.180575,
            2.368750, -4.188888,
            2.243750, -4.197513,
            2.116717, -4.205384,
            1.981250, -4.211513,
            1.856250, -4.218675,
            1.731250, -4.224362,
            1.604334, -4.230793,
            1.468750, -4.234988,
            1.343750, -4.239062,
            1.218750, -4.244662,
            1.093750, -4.247763,
            0.968750, -4.251150,
            0.843750, -4.255163,
            0.718750, -4.257263,
            0.593750, -4.258675,
            0.468750, -4.259037,
            0.343750, -4.259625,
            0.220777, -4.258953,
            0.091212, -4.253839,
            -0.043750, -4.238500,
            -0.168750, -4.226725,
            -0.293750, -4.214112,
            -0.421096, -4.190890,
            -0.558492, -4.165649,
            -0.692517, -4.133186,
            -0.816666, -4.092705,
            -0.927107, -4.037536,
            -1.016373, -3.963820,
            -1.087515, -3.864594,
            -1.120463, -3.756250,
            -1.127925, -3.631250,
            -1.119588, -3.506250,
            -1.099792, -3.387589,
            -1.069435, -3.270340,
            -1.041197, -3.177128,
            -1.007523, -3.070172,
            -0.976159, -2.974503,
            -0.940121, -2.865524,
            -0.906947, -2.757837,
            -0.877026, -2.653520,
            -0.850079, -2.537466,
            -0.828751, -2.423749,
            -0.818213, -2.306250,
            -0.820746, -2.180035,
            -0.840706, -2.058517,
            -0.882493, -1.944993,
            -0.949090, -1.851476,
            -1.052413, -1.787994,
            -1.181250, -1.763250,
            -1.310797, -1.779543,
            -1.436757, -1.826734,
            -1.550737, -1.892275,
            -1.663741, -1.968757,
            -1.770422, -2.061107,
            -1.869270, -2.162067,
            -1.969452, -2.274415,
            -2.060793, -2.383714,
            -2.149543, -2.493881,
            -2.249100, -2.608042,
            -2.346315, -2.710362,
            -2.459372, -2.809898,
            -2.574140, -2.890567,
            -2.695162, -2.957560,
            -2.828680, -3.001834,
            -2.956250, -3.023638,
            -3.081250, -3.027062,
            -3.204969, -3.008018,
            -3.317112, -2.969268,
            -3.417167, -2.906960,
            -3.498445, -2.826046,
            -3.569520, -2.729684,
            -3.631527, -2.617043,
            -3.681707, -2.504595,
            -3.726107, -2.390322,
            -3.757501, -2.279822,
            -3.776113, -2.156250,
            -3.778687, -2.031250,
            -3.769450, -1.906250,
            -3.749964, -1.787515,
            -3.720504, -1.682534,
            -3.681558, -1.587243,
            -3.634459, -1.482119,
            -3.583233, -1.382469,
            -3.535396, -1.294351,
            -3.484996, -1.209378,
            -3.431059, -1.125159,
            -3.372352, -1.034498,
            -3.310673, -0.944272,
            -3.254849, -0.863667,
            -3.195422, -0.773607,
            -3.134182, -0.682198,
            -3.082231, -0.599183,
            -3.033662, -0.510490,
            -2.984110, -0.407219,
            -2.948218, -0.313264,
            -2.918389, -0.208139,
            -2.895888, -0.093750,
            -2.886650, 0.031250,
            -2.895674, 0.157486,
            -2.914737, 0.281250,
            -2.939512, 0.406250,
            -2.971154, 0.532349,
            -3.019147, 0.645422,
            -3.086507, 0.748579,
            -3.161184, 0.848794,
            -3.240592, 0.949126,
            -3.324516, 1.035739,
            -3.411340, 1.123690,
            -3.500920, 1.209803,
            -3.598954, 1.298614,
            -3.686652, 1.398482,
            -3.773237, 1.499092,
            -3.855263, 1.597633,
            -3.924038, 1.698558,
            -3.976087, 1.809692,
            -4.013618, 1.922171,
            -4.042888, 2.043750,
            -4.048162, 2.168750,
            -4.031400, 2.293750,
            -4.004772, 2.398769,
            -3.968617, 2.499889,
            -3.911552, 2.604418,
            -3.835217, 2.690806,
            -3.748927, 2.754314,
            -3.645169, 2.804965,
            -3.531250, 2.837450,
            -3.406250, 2.848763,
            -3.281250, 2.844162,
            -3.156250, 2.829125,
            -3.029410, 2.806442,
            -2.914947, 2.774454,
            -2.802690, 2.736590,
            -2.692109, 2.693244,
            -2.579300, 2.644326,
            -2.467383, 2.592283,
            -2.344984, 2.530016,
            -2.239524, 2.475089,
            -2.126684, 2.413098,
            -2.018457, 2.347489,
            -1.915419, 2.286247,
            -1.814746, 2.224756,
            -1.714864, 2.162079,
            -1.614462, 2.100182,
            -1.514094, 2.038234,
            -1.405653, 1.972662,
            -1.301691, 1.913088,
            -1.189774, 1.862214,
            -1.068750, 1.817538,
            -0.940495, 1.786133,
            -0.818750, 1.767875,
            -0.693750, 1.764200,
            -0.575188, 1.775263,
            -0.457270, 1.803570,
            -0.361566, 1.846802,
            -0.269303, 1.901936,
            -0.187819, 1.967975,
            -0.112427, 2.043729,
            -0.048088, 2.130704,
            0.003823, 2.230158,
            0.035954, 2.321080,
            0.056725, 2.443750,
            0.059912, 2.568750,
            0.042787, 2.693750,
            0.012245, 2.822747,
            -0.024745, 2.935587,
            -0.067573, 3.045872,
            -0.120097, 3.157651,
            -0.182396, 3.280104,
            -0.237495, 3.385420,
            -0.293679, 3.495556,
            -0.350005, 3.610413,
            -0.390488, 3.716577,
            -0.420746, 3.819965,
            -0.435850, 3.943750,
            -0.410612, 4.068211,
            -0.348370, 4.153850,
            -0.257383, 4.216467,
            -0.156250, 4.254938,
            -0.035840, 4.270533,
            0.081250, 4.272412,
            0.206250, 4.256325,
            0.331250, 4.236763,
            0.459824, 4.211611,
            0.582950, 4.180949,
            0.708362, 4.144891,
            0.816867, 4.110190,
            0.922449, 4.074298,
            1.046957, 4.036060,
            1.168750, 4.006537,
            1.294661, 3.989094,
            1.418750, 3.973438,
            1.536490, 3.977357,
            1.650767, 3.997096,
            1.767648, 4.028859,
            1.862972, 4.061399,
            1.972104, 4.097225,
            2.088583, 4.121542,
            2.206250, 4.136538,
            2.331250, 4.117950,
            2.456250, 4.093025,
            2.582400, 4.066526,
            2.707716, 4.030131,
            2.822352, 3.986653,
            2.935400, 3.937474,
            3.045948, 3.882692,
            3.148017, 3.825151,
            3.247171, 3.761382,
            3.345493, 3.686191,
            3.446604, 3.598953,
            3.545291, 3.498202,
            3.626285, 3.398773,
            3.701181, 3.298704,
            3.767896, 3.195292,
            3.824413, 3.097525,
            3.874475, 2.985067,
            3.911944, 2.872546,
            3.942287, 2.756250,
            3.967163, 2.632582,
            3.980513, 2.506250,
            3.987087, 2.381250
        ]
        #Format is x1, y1, yaw1, x2, y2, yaw2, ...
        self.startPositionPoints = [
            3.987762, 2.356250, -1.615067,
            3.982225, 2.231250, -1.685195,
            3.967863, 2.106250, -1.764521,
            3.943029, 1.979670, -1.861590,
            3.900426, 1.837317, -1.968851,
            3.848255, 1.713248, -2.097824,
            3.778186, 1.592840, -2.217434,
            3.690974, 1.477314, -2.329535,
            3.587757, 1.368441, -2.482914,
            3.476889, 1.282631, -2.574162,
            3.368458, 1.213522, -2.704443,
            3.154608, -4.081754, -2.949340,
            3.018750, -4.108200, -2.980089,
            2.889079, -4.129326, -2.997648,
            2.749667, -4.149534, -2.982140,
            2.618750, -4.170588, -3.061862,
            2.493750, -4.180575, -3.075190,
            2.368750, -4.188888, -3.072702,
            2.243750, -4.197513, -3.079707,
            2.116717, -4.205384, -3.096384,
            1.981250, -4.211513, -3.084355,
            1.856250, -4.218675, -3.096124,
            1.731250, -4.224362, -3.090971,
            1.604334, -4.230793, -3.110664,
            1.468750, -4.234988, -3.109004,
            1.343750, -4.239062, -3.096823,
            1.218750, -4.244662, -3.116798,
            1.093750, -4.247763, -3.114499,
            0.968750, -4.251150, -3.109504,
            0.843750, -4.255163, -3.124794,
            0.718750, -4.257263, -3.130293,
            0.593750, -4.258675, -3.138693,
            0.468750, -4.259037, -3.136893,
            0.343750, -4.259625, 3.136124,
            0.220777, -4.258953, 3.102142,
            0.091212, -4.253839, 3.028429,
            -0.043750, -4.238500, 3.047670,
            -0.168750, -4.226725, 3.041033,
            -0.293750, -4.214112, 2.961219,
            -0.421096, -4.190890, 2.959908,
            -0.558492, -4.165649, 2.903952,
            -0.692517, -4.133186, 2.826396,
            -0.816666, -4.092705, 2.678321,
            -0.927107, -4.037536, 2.451316,
            -1.016373, -3.963820, 2.192817,
            -1.087515, -3.864594, 1.866009,
            -1.120463, -3.756250, 1.630426,
            -3.769450, -1.906250, 1.408134,
            -3.749964, -1.787515, 1.297210,
            -3.720504, -1.682534, 1.182812,
            -3.681558, -1.587243, 1.149572,
            -3.634459, -1.482119, 1.095964,
            -3.583233, -1.382469, 1.073445,
            -3.535396, -1.294351, 1.035447,
            -3.484996, -1.209378, 1.001172,
            -3.431059, -1.125159, 0.996150,
            -3.372352, -1.034498, 0.971152,
            -3.310673, -0.944272, 0.965085,
            -3.254849, -0.863667, 0.987524,
            -3.195422, -0.773607, 0.980518,
            -3.134182, -0.682198, 1.011618,
            -3.082231, -0.599183, 1.069787,
            -3.033662, -0.510490, 1.123426,
            -2.984110, -0.407219, 1.205890,
            -2.948218, -0.313264, 1.294315,
            -2.918389, -0.208139, 1.376567,
            -2.895888, -0.093750, 1.497030,
            -2.886650, 0.031250, 1.642159,
            -2.895674, 0.157486, 1.723628,
            -2.914737, 0.281250, 1.766461,
            3.148017, 3.825151, -0.571530,
            3.247171, 3.761382, -0.652875,
            3.345493, 3.686191, -0.711875,
            3.446604, 3.598953, -0.795745,
            3.545291, 3.498202, -0.887223,
            3.626285, 3.398773, -0.928294,
            3.701181, 3.298704, -0.997846,
            3.767896, 3.195292, -1.046652,
            3.824413, 3.097525, -1.151971,
            3.874475, 2.985067, -1.249351,
            3.911944, 2.872546, -1.315569,
            3.942287, 2.756250, -1.372299,
            3.967163, 2.632582, -1.465515,
            3.980513, 2.506250, -1.518245,
            3.987087, 2.381250, -1.543803
        ]
        self.track_width = 0.6750

        # n points is len/2 because list has x and y
        self.n_points = len(self.linePoints)/2

        # we will track which points have been visited
        self.bVisitedPoint = [False] * self.n_points # will set true each time we visit
        self.count_visited_points = 0 # when this equals self.n_points we have finished the race
        self.sum_visited_points_distance = 0.0 # tracks the length (along actual line) of track we visited so far - for live scoring

        # initialize some values
        self.sum_area = 0.0
        self.is_race_finished = False
        self.is_out_of_track = False
        self.is_out_of_sequence = False
        self.prev_position = Point()

        # loop through the points including last connection from end to first
        # sum all the distances to get the total track distance
        # note when we finish race self.sum_visited_points_distance will equal self.track_distance
        self.track_distance = 0.0
        for i1 in range(self.n_points):
            i2 = i1 + 1
            if i2 == self.n_points:
                i2 = 0
            x1 = self.linePoints[i1*2+0]
            y1 = self.linePoints[i1*2+1]
            x2 = self.linePoints[i2*2+0]
            y2 = self.linePoints[i2*2+1]
            dx = x2 - x1
            dy = y2 - y1
            self.track_distance += math.sqrt(dx * dx + dy * dy)

        # we track the two points which are closest - defines the segment we are closest to
        self.position_closest_point_1 = -1
        self.position_closest_point_2 = -1
        self.prev_position_closest_point_1 = -1
        self.prev_position_closest_point_2 = -1

    def get_closest_points(self, position, hint1, hint2): # hints are the two known closest points from before

        # this method will determine the two points closest to the given position
        # it will use prior hint information to optimize
        # we want the closest and the 2nd closest point - to define the closest segment
        best_dist_sq_1 = 9999999.9
        best_index_1 = -1
        best_dist_sq_2 = 9999999.9
        best_index_2 = -1

        # if no hint yet, we search all points, otherwise we search just neighbors
        # if prior known segment was points 4,5 then we search 3,4,5,6
        range_start = 0
        range_end = self.n_points
        if hint1 != -1:
            range_start = min(hint1, hint2) - 1 # inclusive
            range_end = max(hint1, hint2) + 2   # exclusive

        for q in range(range_start,range_end):
            # loop to stay in bounds of array
            i = q
            if i < 0:
                i += self.n_points
            elif i >= self.n_points:
                i -= self.n_points
            x = self.linePoints[i*2+0]
            y = self.linePoints[i*2+1]
            dx = position.x - x
            dy = position.y - y
            dist_sq = dx * dx + dy * dy
            # always assign to the worst dist first
            if best_dist_sq_1 > best_dist_sq_2:
                if dist_sq < best_dist_sq_1:
                    best_dist_sq_1 = dist_sq
                    best_index_1 = i
            elif dist_sq < best_dist_sq_2:
                best_dist_sq_2 = dist_sq
                best_index_2 = i

        # make the ordering always small to big
        if best_index_2 < best_index_1:
            return best_index_2, best_index_1
        else:
            return best_index_1, best_index_2

    def calculate_segment_sum_area(self):
        # this calculation determines the change in sum_area
        # the calculation uses:
        #     position                       Current position of robot
        #     prev_position                  Previous position of robot
        #     position_closest_point_1, position_closest_point_2              Closest segment to position

        # There are 5 cases to consider:
        #     (A) Both points are on the same segment and same side of line
        #     (B) Both points are on the same segment but on opposite sides of the line
        #     (C) Points are on different segments, same side of line, do not cross a segment
        #     (D) Points are on different segments, opposite side of line
        #     (E) Points are on different segments, same side of line, but cross a segment (clipping a corner)

        # For now we will treat all cases to be A or B which will be a good approximation anyways.
        # Therefore act as though the previous point is on the current segment, even if it is not.
        # Also the points themselves are an approximation since they don't have perfect resolution
        # So handling cases like E with perfect accuracy will be somewhat pointless since the 'corners' don't really exist
        # Also the line is usually changing slowly and we have detailed resolution so angle changes between segments will be small

        # We have also added a 'thickness' to the track defined by self.track_width
        # If distance from line is less than half this distance, it's considered perfect and no penalty.

        # Define half width of track
        half_width = self.track_width * 0.5

        # Define the segment
        sx1 = self.linePoints[self.position_closest_point_1*2+0]
        sy1 = self.linePoints[self.position_closest_point_1*2+1]
        sx2 = self.linePoints[self.position_closest_point_2*2+0]
        sy2 = self.linePoints[self.position_closest_point_2*2+1]

        # define the two points
        x1 = self.prev_position.x
        y1 = self.prev_position.y
        x2 = self.position.x
        y2 = self.position.y

        # get segment vector
        sdx = sx2 - sx1
        sdy = sy2 - sy1

        # Get length of segment
        length = math.sqrt(sdx * sdx + sdy * sdy)

        # If robot does not move we have 0 area
        if length == 0: # TODO: Determine this as an epsilon
            return 0

        # calculate the normal vector of the segment
        inv_length = 1.0 / length
        normx = sdx * inv_length
        normy = sdy * inv_length

        # calculate dotP of the vector from segment beginning to robot points, relative to the normalized segment vector
        dotP1 = normx * (x1-sx1) + normy * (y1-sy1)
        dotP2 = normx * (x2-sx1) + normy * (y2-sy1)

        # determine the closest point on the line for each of the points
        ix1 = sx1 + normx * dotP1
        iy1 = sy1 + normy * dotP1
        ix2 = sx1 + normx * dotP2
        iy2 = sy1 + normy * dotP2

        # determine vector from intersect point to robot point
        vx1 = x1 - ix1
        vy1 = y1 - iy1
        vx2 = x2 - ix2
        vy2 = y2 - iy2

        # determine lengths of the vectors
        vlength1 = math.sqrt(vx1*vx1 + vy1*vy1)
        vlength2 = math.sqrt(vx2*vx2 + vy2*vy2)

        # check for out of bounds
        if vlength1 > self.out_of_track_radius:
            self.is_out_of_track = True

        # determine if points are on opposite sides of the segment, where the dot of vectors will be negative sign
        dot_vectors = (vx1 * vx2) + (vy1 * vy2)

        # determine the length of vector on the segment connecting the two intersects
        # this is just the abs difference of the dots
        A = abs(dotP1 - dotP2)

        if vlength1 < half_width or vlength2 < half_width:
            return 0 # for simplicity we are considered inside track if at least one point is inside
        elif dot_vectors < 0:
            return 0 # with new track width a cross over is always considered 0 cost

            '''
            # We have two triangles on opposite sides
            # The bases have total length A and ratio vlength1/(vlength1 + vlength2)
            ratio = vlength1 / (vlength1 + vlength2)
            base1 = A * ratio
            base2 = A - base1
            triangle1 = 0.5 * base1 * vlength1
            triangle2 = 0.5 * base2 * vlength2
            return triangle1 + triangle2
            '''
        else:
            # we have a trapezoid figure with base A, sides vlength1, vlength2, sides are parallel and perpendicular to A
            # so area is a square + triangle
            square = A * min(vlength1, vlength2)
            triangle = 0.5 * A * abs(vlength1 - vlength2)
            return square + triangle - A * half_width # subtract the rectangle representing inside the track

    def visit_point(self, index):
        if not self.bVisitedPoint[index]:
            # point not visited, so set it True and increment counter
            self.bVisitedPoint[index] = True
            self.count_visited_points += 1

            # Now we also want to accumulate a visited 'distance' along the actual line
            # So this distance will eventually be track distance when we finish
            # We do this to debug but also allows us to have a live score which updates as we go.
            prior = index - 1
            if prior == -1:
                prior = self.n_points - 1
            x1 = self.linePoints[index*2+0]
            y1 = self.linePoints[index*2+1]
            x2 = self.linePoints[prior*2+0]
            y2 = self.linePoints[prior*2+1]
            dx = x2 - x1
            dy = y2 - y1
            length = math.sqrt(dx * dx + dy * dy)
            self.sum_visited_points_distance += length

    def update(self, position):
        # first update so we know closest point
        self.position = position
        self.position_closest_point_1, self.position_closest_point_2 = self.get_closest_points(position, self.prev_position_closest_point_1, self.prev_position_closest_point_2)

        # now calculate increase in sum_area if this is not the first loop
        if self.prev_position_closest_point_1 != -1:
            self.sum_area += self.calculate_segment_sum_area()

            # inspect the change in sequence - if it's excessive than we are out of sequence
            delta_sequence = abs(self.position_closest_point_1 - self.prev_position_closest_point_1)
            if delta_sequence >= self.n_points/2:
                delta_sequence -= self.n_points # wrap around
            if delta_sequence > self.maximum_point_skip_allowed: # did we skip too many points somehow
                self.is_out_of_sequence = True

            # track the visited points, looping around if necessary
            if self.position_closest_point_1 != self.prev_position_closest_point_1:
                delta = self.position_closest_point_1 - self.prev_position_closest_point_1
                if delta > 0:
                    if delta < self.n_points/2:
                        direction = 1
                    else:
                        direction = -1
                else:
                    if -delta < self.n_points/2:
                        direction = -1
                    else:
                        direction = 1
                scan_index = self.prev_position_closest_point_1
                while True:
                    scan_index += direction
                    if scan_index < 0:
                        scan_index += self.n_points
                    elif scan_index >= self.n_points:
                        scan_index -= self.n_points
                    self.visit_point(scan_index)
                    if scan_index == self.position_closest_point_1:
                        break
        else:
            self.visit_point(self.position_closest_point_1)

        # save prev info so we have it for next cycle
        self.prev_position = position
        self.prev_position_closest_point_1 = self.position_closest_point_1
        self.prev_position_closest_point_2 = self.position_closest_point_2

    def get_sum_area(self):
        return self.sum_area

    def get_track_distance(self):
        return self.track_distance

    def get_race_finished(self):
        return self.count_visited_points == self.n_points

    def get_out_of_track(self):
        return self.is_out_of_track

    def get_out_of_sequence(self):
        return self.is_out_of_sequence

    def calculate_score(self, time):
        if time > 0:
            base_score = self.sum_visited_points_distance / time
            q = 1.0 / 15.0 # q is tuning parmameter - your average distance from line (per unit length not unit time)
            penalty_factor = 1.0 / ( 1.0 + self.sum_area / (q * self.sum_visited_points_distance) )
            return base_score * penalty_factor
        else:
            return 0

